﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "Projektni_zadatak_head.h"



void pocetni_izbornik() {

	printf("Za odabir zeljene radnje unesite broj: \n\n");
	printf("1. Prijava klijenta\n");
	printf("2. Prijava poslodavca\n");
	printf("3. Kraj programa\n");
}

void izlaz() {
	char odabir;
	system("cls");
	printf("\nJeste li sigurni da zelite izaci? (y/n)");
	odabir = getch();
	if (odabir == 'y' || odabir == 'Y') {
		exit(0);
	}
}

void izbornik_klijenta() {// kada se na pocetnom izborniku pritisne: 1. Prijava korisnika

	printf("\n******DOBRODOSLU U ZALAGAONICU TAMBURASKIH INSTRUMENATA******\n\n");

	printf("Vi ste klijent\n");
	printf("Za odabir zeljene radnje unesite broj: \n\n");
	printf("1. Zalaganje instrumenta\n");
	printf("2. Kupnja instrumenta\n");
	printf("3. Odjava klijenta\n");
}

KLIJENT* zauzimanje_memorije(int broj) {

	KLIJENT* korisnik = NULL;
	korisnik = (KLIJENT*)calloc(broj, sizeof(KLIJENT));
	if (korisnik == NULL) {
		return NULL;
	}
	else {
		return korisnik;
	}
}

void izbor_instrumenata_zalaganje() {

	printf("Utipkajte broj za instrument kojeg zelite zaloziti: \n");
	printf("1. Prim\n");
	printf("2. Basprim\n");
	printf("3. Bas\n");
	printf("4. Celo\n");
	printf("5. Kontra\n");
}

int random_broj(int DG, int GG) {
	int broj;
	broj = DG + (float)rand() / RAND_MAX * (GG - DG);
	return broj;
}


void ispis_klijenata() {
	FILE* fp = NULL;
	fp = fopen("datoteka_klijenata.bin", "rb");
	if (fp == NULL) {
		printf("\nGreska!");
	}
	else {
		int broj_korisnika;
		fread(&broj_korisnika, sizeof(int), 1, fp);
		KLIJENT* korisnik = zauzimanje_memorije(broj_korisnika);
		if (korisnik == NULL) {
			printf("\nGreska!");
		}
		else {
			fread(korisnik, sizeof(KLIJENT), broj_korisnika, fp);//ucita u ram iz datoteke
			fclose(fp);
			printf("\nZalagaonicu je posjetilo %d klijenata\n\n", broj_korisnika);
			for (int i = 0; i < broj_korisnika; i++) {//ispis iy rama
				printf("Ime: %s", (korisnik + i)->ime);
				printf("\nPrezime: %s", (korisnik + i)->prezime);
				printf("\nOIB: %s", (korisnik + i)->oib);
				printf("\nSvota: %.2f kn", (korisnik + i)->svota);
				printf("\n\n");
			}
			free(korisnik);
			korisnik = NULL;
		}
	}
}

void izbor_instrumenata_kupnje() {

	printf("Utipkajte broj za instrument kojeg zelite kupiti: \n");
	printf("1. Prim\n");
	printf("2. Basprim\n");
	printf("3. Bas\n");
	printf("4. Celo\n");
	printf("5. Kontra\n");
}

void izbornik_poslodavca() { // poslodavac mora unjeti određeni pin

	printf("Vi ste POSLODAVAC\n\n");
	printf("Za odabir zeljene radnje unesite broj: \n");
	printf("1. Pregled instrumenata\n");
	printf("2. Pregled klijenata\n");
	printf("3. Trenutno financijsko stanje\n");
	printf("4. Sortiranje po svoti od najveceg do najmanjeg.\n");
	printf("5. Pronalazak po IOB-u\n");
	printf("6. Odjava poslodavca\n");
}


void sortiranje_po_svoti() {
	FILE* fp = NULL;
	int broj;
	KLIJENT pom;
	fp = fopen("datoteka_klijenata.bin", "rb");
	if (fp == NULL) {
		printf("\nGreska!");
	}
	else {
		fread(&broj, sizeof(int), 1, fp);
		if (broj == 0) {
			printf("\nNema korisnika za sortiranje!");
		}
		else {
			KLIJENT* korisnici = zauzimanje_memorije(broj);
			if (korisnici == NULL) {
				printf("\nGreska");
			}
			else {
				fread(korisnici, sizeof(KLIJENT), broj, fp);
				fclose(fp);

				//sortiranje selection sort
				int max;
				for (int i = 0; i < broj; i++) {
					max = i;
					for (int j = i + 1; j < broj; j++) {
						if ((korisnici + j)->svota > (korisnici + max)->svota) {
							max = j;
						}
					}

					//zamijena imena
					strcpy(pom.ime, (korisnici + i)->ime);
					strcpy((korisnici + i)->ime, (korisnici + max)->ime);
					strcpy((korisnici + max)->ime, pom.ime);

					//zamjena prezimena
					strcpy(pom.prezime, (korisnici + i)->prezime);
					strcpy((korisnici + i)->prezime, (korisnici + max)->prezime);
					strcpy((korisnici + max)->prezime, pom.prezime);

					//zamjena OIB-a
					strcpy(pom.oib, (korisnici + i)->oib);
					strcpy((korisnici + i)->oib, (korisnici + max)->oib);
					strcpy((korisnici + max)->oib, pom.oib);

					//zamjena svote
					pom.svota = (korisnici + i)->svota;
					(korisnici + i)->svota = (korisnici + max)->svota;
					(korisnici + max)->svota = pom.svota;

				}

				//upis u datoteku
				fp = fopen("datoteka_klijenata.bin", "wb");
				if (fp == NULL) {
					printf("\nGreska!");
				}
				else {
					fwrite(&broj, sizeof(int), 1, fp);
					fwrite(korisnici, sizeof(KLIJENT), broj, fp);
					fclose(fp);
					free(korisnici);
					korisnici = NULL;
				}
				printf("\nSortiranje uspjesno obavljeno!\n");
			}
		}
	}
}

void pronalazak() {
	FILE* fp = NULL;
	int broj;
	KLIJENT* korisnici = NULL;
	int f = 0;
	char oib[20];
	fp = fopen("datoteka_klijenata.bin", "rb");
	if (fp == NULL) {
		printf("\nGreska!");
	}
	else {
		fread(&broj, sizeof(int), 1, fp);
		korisnici = zauzimanje_memorije(broj);
		if (korisnici == NULL) {
			printf("\nGreska!");
		}
		else {
			fread(korisnici, sizeof(KLIJENT), broj, fp);
			fclose(fp);
			system("cls");
			printf("Unesite OIB trazenog klijenta: ");
			scanf(" %19[^\n]", oib);
			for (int i = 0; i < broj; i++) {
				if (strcmp((korisnici + i)->oib, oib) == 0) {
					system("cls");
					//ispis trazenog
					printf("Trazeni korisnik:\n");
					printf("Ime: %s", (korisnici + i)->ime);
					printf("\nPrezime: %s", (korisnici + i)->prezime);
					printf("\nOIB: %s", (korisnici + i)->oib);
					printf("\nSvota: %.2f kn", (korisnici + i)->svota);
					f = 1;
					break;
				}
			}
		}
	}
	if (f == 0) {
		printf("Korisnik sa obibom %s ne postoji!", oib);
	}
}