﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "Projektni_zadatak_head.h"


int main() {

	srand((unsigned)time(NULL));
	FILE* fp = NULL;
	fp = fopen("datoteka_klijenata.bin", "rb");// otvara dat za citanje
	if (fp == NULL) {// ako ne postoji 
		fp = fopen("datoteka_klijenata.bin", "wb");//kreira datoteku
		if (fp == NULL) {// ako ne uspije kreirati
			printf("\nDoslo je do pogreske!");
			return 1;//izlazi van
		}
		else {
			int nula = 0;// ako uspije na pocetku upisuje 0
			fwrite(&nula, sizeof(int), 1, fp);//ovo 0
			fclose(fp);
		}
	}

	else {


		int financijsko_stanje = random_broj(40000, 80000);
		int prim = random_broj(4, 15);
		int basprim = random_broj(4, 15);
		int bas = random_broj(4, 15);
		int celo = random_broj(4, 15);
		int kontra = random_broj(4, 15);

		while (1) {

			char prvi_odabir;
			do {
				system("cls");
				pocetni_izbornik();
				prvi_odabir = getch();

				if (prvi_odabir != '1' && prvi_odabir != '2' && prvi_odabir != '3') {
					printf("\nPogresan unos!");
				}
			} while (prvi_odabir != '1' && prvi_odabir != '2' && prvi_odabir != '3');

			switch (prvi_odabir) {


			case '1':  //Prijava klijenta

				system("cls");
				fp = fopen("datoteka_klijenata.bin", "rb+");//citanje i pisanje
				if (fp == NULL) {
					printf("\nGreska!");
					printf("\nPritisnite bilo koju tipku za nastavak...");
					getch();
				}
				else {
					int broj_korisnika;
					rewind(fp);// vraca na pocetak bin datoteke
					fread(&broj_korisnika, sizeof(int), 1, fp);//citanje broja korisnika
					broj_korisnika++;
					rewind(fp);
					fwrite(&broj_korisnika, sizeof(int), 1, fp);
					fseek(fp, 0, SEEK_END);//ide na kraj datoteka i tamo doda korisnika

					KLIJENT* korisnik = zauzimanje_memorije(1);//prijava jednog klijenta
					if (korisnik == NULL) {
						printf("\nDoslo je do greske kod zauzimanja memorije\n");
					}
					else {//upisivanje podataka klijenta i njihovo upisivanje u dokument
						system("cls");
						printf("Unesite Vase ime: \n");
						scanf(" %19s", korisnik->ime);

						printf("Unesite Vase prezime: \n");
						scanf(" %19s", korisnik->prezime);

						printf("Unesite Vas oib: \n");
						scanf(" %19s", korisnik->oib);

						printf("Unesite Vas novac: \n");
						scanf(" %f", &korisnik->svota);
					}

					//Radnje
					char drugi_odabir;
					do {
						system("cls");
						izbornik_klijenta();

						do {
							drugi_odabir = getch();
							if (drugi_odabir < '1' || drugi_odabir > '3') {
								printf("\nPogresan odabir! Pokusajte ponovo: ");
							}
						} while (drugi_odabir < '1' || drugi_odabir > '3');

						switch (drugi_odabir) {

						case '1': // Zalaganje instrumenta
							system("cls");
							izbor_instrumenata_zalaganje();
							char odabir_zalaganja;
							do {
								odabir_zalaganja = getch();
								if (odabir_zalaganja < '1' || odabir_zalaganja > '5') {
									printf("\nPogresan odabir! Pokusajte ponovo: ");
								}
							} while (odabir_zalaganja < '1' || odabir_zalaganja > '5');

							switch (odabir_zalaganja) {

							case '1': // Zalaganje Prima

								system("cls");
								printf("Za zalaganje prima mozete dobiti 2 000 kn\n");
								printf("Jeste li sigurni da zelite zaloziti prim? (y/n)");
								char potvrda1 = getch();
								if (potvrda1 == 'y' || potvrda1 == 'Y') {
									if (financijsko_stanje < 10000) {
										printf("\nZao nam je, ali ne možemo Vam isplatiti novac za Vaš instrument.");
									}
									else {
										printf("\nUspjesno ste zalozii Vas instrument.\n");
										financijsko_stanje -= 2000;
										korisnik->svota += 2000;
										prim++;
									}
								}

								break;

							case '2': //Zalaganje Basprima

								system("cls");
								printf("Za zalaganje basprima mozete dobiti 3 000 kn\n");
								printf("Jeste li sigurni da zelite zaloziti basprim? (y/n)");
								char potvrda2 = getch();
								if (potvrda2 == 'y' || potvrda2 == 'Y') {
									if (financijsko_stanje < 10000) {
										printf("\nZao nam je, ali ne možemo Vam isplatiti novac za Vaš instrument.");
									}
									else {
										printf("\nUspjesno ste zalozii Vas instrument.\n");
										financijsko_stanje -= 3000;
										korisnik->svota += 3000;
										basprim++;
									}
								}

								break;

							case '3': // Zalaganje Basa

								system("cls");
								printf("Za zalaganje basa mozete dobiti 6 000 kn\n");
								printf("Jeste li sigurni da zelite zaloziti bas? (y/n)");
								char potvrda3 = getch();
								if (potvrda3 == 'y' || potvrda3 == 'Y') {
									if (financijsko_stanje < 10000) {
										printf("\nZao nam je, ali ne možemo Vam isplatiti novac za Vaš instrument.");
									}
									else {
										printf("\nUspjesno ste zalozii Vas instrument.\n");
										financijsko_stanje -= 6000;
										korisnik->svota += 6000;
										bas++;
									}
								}

								break;

							case '4': // Zalaganje cela

								system("cls");
								printf("Za zalaganje cela mozete dobiti 5 000 kn\n");
								printf("Jeste li sigurni da zelite zaloziti celo? (y/n)");
								char potvrda4 = getch();
								if (potvrda4 == 'y' || potvrda4 == 'Y') {
									if (financijsko_stanje < 10000) {
										printf("\nZao nam je, ali ne možemo Vam isplatiti novac za Vaš instrument.");
									}
									else {
										printf("\nUspjesno ste zalozii Vas instrument.\n");
										financijsko_stanje -= 5000;
										korisnik->svota += 5000;
										celo++;
									}
								}

								break;

							case '5': // Zalaganje kontre

								system("cls");
								printf("Za zalaganje kontre mozete dobiti 4 000 kn\n");
								printf("Jeste li sigurni da zelite zaloziti kontru? (y/n)");
								char potvrda5 = getch();
								if (potvrda5 == 'y' || potvrda5 == 'Y') {
									if (financijsko_stanje < 10000) {
										printf("\nZao nam je, ali ne možemo Vam isplatiti novac za Vaš instrument.");
									}
									else {
										printf("\nUspjesno ste zalozii Vas instrument.\n");
										financijsko_stanje -= 4000;
										korisnik->svota += 4000;
										kontra++;
									}
								}

								break;

							}
				
							break;


						case '2': // Kupnja instrumenta

							system("cls");
							izbor_instrumenata_kupnje();
							char odabir_kupnje;
							do {
								odabir_kupnje = getch();
								if (odabir_kupnje < '1' || odabir_kupnje > '5') {
									printf("\nPogresan odabir! Pokusajte ponovo: ");
								}
							} while (odabir_kupnje < '1' || odabir_kupnje > '5');

							switch (odabir_kupnje) {

							case '1': // Kupnja prima

								system("cls");
								if (prim > 0) {
									printf("Za kupnju prima morate platiti 3 000 kn\n");
									printf("Jeste li sigurni da zelite kupiti prim? (y/n)");
									char potvrda1 = getch();
									if (potvrda1 == 'y' || potvrda1 == 'Y') {
										if (korisnik->svota >= 3000) {
											financijsko_stanje += 3000;
											korisnik->svota -= 3000;
											prim--;
											printf("\nZahvaljujemo Vam na kupnji instrumenta!");
										}
										else {
											printf("\nNemate dovoljno novaca da bi kupili instrument\n");
										}
									}
								}
								else {
									printf("\nNemamo instrument na raspolaganju!");
								}

								break;

							case '2': // Kupnja Basprima

								system("cls");
								if (basprim > 0) {
									printf("Za kupnju basprma morate platiti 4 000 kn\n");
									printf("Jeste li sigurni da zelite kupiti basprim? (y/n)");
									char potvrda1 = getch();
									if (potvrda1 == 'y' || potvrda1 == 'Y') {
										if (korisnik->svota >= 4000) {
											financijsko_stanje += 4000;
											korisnik->svota -= 4000;
											basprim--;
											printf("\nZahvaljujemo Vam na kupnji instrumenta!");
										}
										else {
											printf("\nNemate dovoljno novaca da bi kupili instrument\n");
										}
									}
								}
								else {
									printf("\nNemamo instrument na raspolaganju!");
								}
								break;

							case '3': // Kupnja Basa

								system("cls");
								if (bas > 0) {
									printf("Za kupnju basa morate platiti 7 000 kn\n");
									printf("Jeste li sigurni da zelite kupiti bas? (y/n)");
									char potvrda1 = getch();
									if (potvrda1 == 'y' || potvrda1 == 'Y') {
										if (korisnik->svota >= 7000) {
											financijsko_stanje += 7000;
											korisnik->svota -= 7000;
											bas--;
											printf("\nZahvaljujemo Vam na kupnji instrumenta!");
										}
										else {
											printf("\nNemate dovoljno novaca da bi kupili instrument\n");
										}
									}
								}
								else {
									printf("\nNemamo instrument na raspolaganju!");
								}
								break;

							case '4': // Kupnja cela

								system("cls");
								if (celo > 0) {
									printf("Za kupnju cela morate platiti 6 000 kn\n");
									printf("Jeste li sigurni da zelite kupiti celo? (y/n)");
									char potvrda1 = getch();
									if (potvrda1 == 'y' || potvrda1 == 'Y') {
										if (korisnik->svota >= 6000) {
											financijsko_stanje += 6000;
											korisnik->svota -= 6000;
											celo--;
											printf("\nZahvaljujemo Vam na kupnji instrumenta!");
										}
										else {
											printf("\nNemate dovoljno novaca da bi kupili instrument\n");
										}
									}
								}
								else {
									printf("\nNemamo instrument na raspolaganju!");
								}
								break;

							case '5': // kontra

								system("cls");
								if (kontra > 0) {
									printf("Za kupnju kontre morate platiti 5 000 kn\n");
									printf("Jeste li sigurni da zelite kupiti kontru? (y/n)");
									char potvrda1 = getch();
									if (potvrda1 == 'y' || potvrda1 == 'Y') {
										if (korisnik->svota >= 5000) {
											financijsko_stanje += 5000;
											korisnik->svota -= 5000;
											kontra--;
											printf("\nZahvaljujemo Vam na kupnji instrumenta!");
										}
										else {
											printf("\nNemate dovoljno novaca da bi kupili instrument\n");
										}
									}
								}
								else {
									printf("\nNemamo instrument na raspolaganju!");
								}

								break;

							}

							break;
						}

						printf("\nPritisnite bilo koju tipku za nastavak...");
						getch();

					} while (drugi_odabir != '3');

					fwrite(korisnik, sizeof(KLIJENT), 1, fp); //upisao na kraj bin datoteke
					free(korisnik);
					korisnik = NULL;
					fclose(fp);

				}

				break;



			case '2':  // Prijava poslodavca

				system("cls");
				int lozinka = 1234;
				int upis_lozinke;
				printf("Unesite lozinku: ");
				scanf(" %d", &upis_lozinke);

				if (lozinka == upis_lozinke) {
					char odabir_poslodavca;

					do {
						system("cls");
						izbornik_poslodavca();
						do {
							odabir_poslodavca = getch();
							if (odabir_poslodavca < '1' || odabir_poslodavca > '6') {
								printf("\nPogresan odabir! Pokusajte ponovo: ");
							}
						} while (odabir_poslodavca < '1' || odabir_poslodavca > '6');

						switch (odabir_poslodavca) {
						case '1': //pregled instrumenata

							system("cls");
							printf("\nPosjedujete %d primova", prim);
							printf("\nPosjedujete %d basprimova", basprim);
							printf("\nPosjedujete %d basova", bas);
							printf("\nPosjedujete %d cela", celo);
							printf("\nPosjedujete %d kontra", kontra);

							break;

						case '2': // pregled klijenata

							ispis_klijenata();

							break;

						case '3': //financijsko stanje

							system("cls");
							printf("Financijsko stanje zalagaonice je %d kn\n", financijsko_stanje);

							break;

						case '4': // sortiranje po svoti							

							sortiranje_po_svoti();

							break;

						case '5': // pronalazak po oib-u

							pronalazak();

							break;

						}

						printf("\n\nPritisnite bilo koju tipku za nastavak...");
						getch();

					} while (odabir_poslodavca != '6');
				}
				else {
					printf("\nPogresna lozinka!");
					printf("\n\nPritisnite bilo koju tipku za nastavak...");
					getch();
				}

				break;


			case '3': //kraj
				izlaz();
				break;


			}

		}
	}

	return 0;
}

