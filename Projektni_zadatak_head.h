#ifndef HEAD_H
#define HEAD_H

typedef struct klijent {

	char ime[20];
	char prezime[20];
	char oib[20];
	float svota;

}KLIJENT;

void pocetni_izbornik();

void izlaz();

void izbornik_klijenta();

KLIJENT* zauzimanje_memorije(int);

void izbor_instrumenata_zalaganje();

int random_broj(int, int);

void ispis_klijenata();

void izbor_instrumenata_kupnje();

void izbornik_poslodavca();

void sortiranje_po_svoti();

void pronalazak();

#endif

